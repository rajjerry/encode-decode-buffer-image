var obj = {
    name: 'raj',
    userName: 'rajjerry',
    skill: 'nodejs'
}
var buffer1 = Buffer.from(JSON.stringify(obj));
console.log('Buffer Reading : ', buffer1.toString());
console.log('Convert buffer to JSON <Bufname>.toJSON() ');

const file = require('fs');

file.readFile('dagdu.jpg', (error, data) => {
    if (error) console.log('Error while reading file1: ', error);
    let dagdu = Buffer.from(data);
    console.log('dagdu image buf length :', dagdu.length);
    file.readFile('om.jpg', (error2, data2) => {
        if (error) console.log('Error while reading file2: ', error2);
        let om = Buffer.from(data2);
        console.log('om image buf length : ', om.length);
        let newBuffer = Buffer.alloc(6000000);
        newBuffer = Buffer.concat([dagdu, om]);
        console.log('COncate neBuff length : ', newBuffer.length);
        file.writeFile('out.jpg', newBuffer, (err, data) => {
            if (err) console.log(err);
            let comPareBuf = Buffer.compare(newBuffer, dagdu)
            console.log('Output image and dagdu image - buffer comparison : ', comPareBuf);
        })
    })
})