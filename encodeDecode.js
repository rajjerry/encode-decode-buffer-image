const bufAlloc = Buffer.alloc(10);
console.log('Buf initlize with alloc method :', bufAlloc);
const bufUnsafe = Buffer.allocUnsafe(10);
console.log('Buf unsafe : ', bufUnsafe);
const bufFrom = Buffer.from("My name is Raj Jariwala.");
console.log('This is the string buffer : toString function Bydefault takes utf8 : ', bufFrom.toString());
console.log('In chinese utf16le : ', bufFrom.toString('utf16le'));
const file = require('fs');


var returnBase64Str = base64_encode('dagdu.jpg');
console.log('Encoded String : ', returnBase64Str.substr(0, 200));
base64_decode(returnBase64Str, 'copy.jpg');

function base64_encode(imageUrl) {
    console.log('Encode the image');
    var bitmap = file.readFileSync(imageUrl);
    return Buffer.from(bitmap).toString('base64');
}

function base64_decode(base64EncodeStr, imageUrl) {
    console.log('Decode the image and new copy will be store');
    var bitmap = Buffer.from(base64EncodeStr, 'base64');
    file.writeFileSync(imageUrl, bitmap);
}